import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> _items = [];

  late pw.Document pdf;

  @override
  void initState() {
    super.initState();
    _initialize();
  }

  void _initialize() {
    _generateItems();
    _generatePdf();
  }

  void _generateItems() {
    _items.addAll(
      List.generate(40, (i) {
        final length = Random.secure().nextInt(10) + 20;

        return '$i.${_generateString(length)}';
      }),
    );
  }

  String _generateString(int length) {
    return List.generate(length, (i) {
      // a-z = 97-122
      final charCode = Random.secure().nextInt(26) + 97;

      return String.fromCharCode(charCode);
    }).join();
  }

  Future<void> _generatePdf() async {
    final svgRaw = await rootBundle.loadString('assets/duck.svg');

    pdf = pw.Document();

    pdf.addPage(pw.MultiPage(
      pageFormat: PdfPageFormat.a4,
      margin: pw.EdgeInsets.zero,
      header: (context) {
        return pw.Container(
          padding: const pw.EdgeInsets.all(8),
          color: PdfColors.grey,
          child: pw.Row(
            children: [
              pw.Container(
                width: 128,
                height: 128,
                child: pw.FlutterLogo(),
              ),
              pw.Text(
                'Header',
                style: const pw.TextStyle(fontSize: 64),
              ),
            ],
          ),
        );
      },
      build: (context) {
        return [
          pw.Row(
            children: [
              pw.SvgImage(
                svg: svgRaw,
                width: 64,
                height: 64,
                fit: pw.BoxFit.contain,
              ),
              pw.Text(
                'Elit occaecat ut adipisicing in deserunt magna quis Lorem ea voluptate.',
                style: pw.TextStyle(
                  color: PdfColor.fromHex('#FF0000'),
                  fontSize: 24,
                ),
              ),
            ],
          ),
          pw.Table(
            children: [
              // HEADER, Repeated on every page
              pw.TableRow(
                repeat: true, // <--- 'Repeat this row on all pages'
                children: [
                  pw.Container(
                    padding: const pw.EdgeInsets.all(8),
                    child: pw.Text(
                      '#',
                      style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  pw.Container(
                    padding: const pw.EdgeInsets.all(8),
                    child: pw.Text(
                      'Value',
                      style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold, fontSize: 16),
                    ),
                  ),
                ],
              ),
              // ROWS
              ...List.generate(
                _items.length,
                (i) {
                  final split = _items[i].split('.');

                  return pw.TableRow(
                    children: [
                      pw.Container(
                        padding: const pw.EdgeInsets.all(8),
                        child: pw.Text(
                          '$i',
                        ),
                      ),
                      pw.Container(
                        padding: const pw.EdgeInsets.all(8),
                        child: pw.RichText(
                          text: pw.TextSpan(
                            children: [
                              pw.TextSpan(
                                style: const pw.TextStyle(color: PdfColors.red),
                                text: '${split[0]}.',
                              ),
                              pw.TextSpan(
                                style: pw.TextStyle(
                                  color: PdfColors.blue,
                                  fontStyle: pw.FontStyle.italic,
                                ),
                                text: split[1],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
          pw.Container(
            margin: const pw.EdgeInsets.only(top: 16),
            padding: const pw.EdgeInsets.all(8),
            decoration:
                pw.BoxDecoration(border: pw.Border.all(color: PdfColors.black)),
            child: pw.Row(
              children: [
                pw.Text(
                  'Summary: ',
                  style: pw.TextStyle(fontWeight: pw.FontWeight.bold),
                ),
                pw.Text(
                  'Anim ut velit nostrud velit reprehenderit culpa ut culpa ipsum.',
                ),
              ],
            ),
          ),
        ];
      },
      footer: (context) {
        return pw.Container(
          padding: const pw.EdgeInsets.all(8),
          color: PdfColors.grey,
          child: pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
            children: [
              pw.Text(
                'Footer',
                style: const pw.TextStyle(fontSize: 24),
              ),
              pw.Text(
                '${context.pageNumber}/${context.pagesCount}',
                style: const pw.TextStyle(fontSize: 24),
              ),
            ],
          ),
        );
      },
    ));

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PDF'),
        actions: [
          IconButton(
            onPressed: _generatePdf,
            icon: const Icon(Icons.refresh),
          ),
        ],
      ),
      body: PdfPreview(
        useActions: false,
        build: (format) => pdf.save(),
      ),
    );
  }
}
